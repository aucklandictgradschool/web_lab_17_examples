package ictgradschool.web.simplewebapp.db;

public class Configuration {
    static final String MYSQL_DBNAME = "your_db_name";
    static final int MYSQL_PORT = 3306;
    static final String MYSQL_HOSTNAME = "mysql1.sporadic.co.nz";
    static final String MYSQL_USERNAME = "your_user_name";
    static final String MYSQL_PASSWORD = "your_passwsord";
}
